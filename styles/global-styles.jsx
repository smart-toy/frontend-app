import React from 'react'
import { GlobalStyles } from '@mui/material'


const UserGlobalStyles = () => (
    <GlobalStyles
        styles={{
            body: {
                margin: 0,
                backgroundColor: '#ffffff',
            },

            // scrollbar
            '&::-webkit-scrollbar': {
                width: 4,
                background: 'transparent',
            },
            '&::-webkit-scrollbar-thumb': {
                width: 4,
                background: '#29b2e3',
            },
            '&::-webkit-scrollbar-track': {
                background: 'transparent',
            },

            // Text selection color
            // '::selection': {
            //     background: '#57BA84',
            // },
            // '::-moz-selection': {
            //     background: '#57BA84',
            // },

            // Input arrows
            'input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button ': {
                WebkitAppearance: 'none',
            },

            'input[type="number"], input[type="number"]:hover, input[type="number"]:focus': {
                appearance: 'none',
                MozAppearance: 'textfield',
            },

        }}
    />
)

export default UserGlobalStyles
