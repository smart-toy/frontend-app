import { CssBaseline, ThemeProvider } from '@mui/material'
import theme from '../styles/theme'
import UserGlobalStyles from '../styles/global-styles'
import { QueryClient, QueryClientProvider } from 'react-query'
import NProgress from "nprogress";
import Router from "next/router";
import "nprogress/nprogress.css";


NProgress.configure({ showSpinner: true });

Router.events.on("routeChangeStart", () => {
    NProgress.start();
});
Router.events.on("routeChangeComplete", () => {
    NProgress.done();
});

const queryClient = new QueryClient()

function MyApp({ Component, pageProps }) {
    return (
        <ThemeProvider theme={theme}>
            <UserGlobalStyles theme={theme}/>
            <CssBaseline/>
            <QueryClientProvider client={queryClient}>
                <Component {...pageProps} />
            </QueryClientProvider>
        </ThemeProvider>
    )
}

export default MyApp
