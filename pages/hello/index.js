import React, { useEffect, useRef } from 'react'
import { useRouter } from 'next/router'
import { Box } from '@mui/material'
import usePageRedirect from '../../utils/use-page-redirect'


const AppPage = () => {

    const toApp = usePageRedirect('/auth/group')
    const ref = useRef(null)

    useEffect(() => {
        ref.current.style.opacity = 1
        setTimeout(() => {
            toApp()
        }, 5000)
    }, [])

    return (
        <Box
            ref={ref}
            sx={{
                height: '100vh',
                width: '100vw',
                backgroundImage: `url(https://sun9-57.userapi.com/impg/5IiH47MzcyiVaLqPRBoxlxU_cVTEQbUtN1ODeQ/AwMF_TiPnrI.jpg?size=730x661&quality=96&sign=a3f819db14b49d1ee690fb1428cd5887&type=album)`,
                backgroundSize: '50%, 50%',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center center',
                opacity: 0,
                transition: 'opacity 2.5s',
            }}
        />
    )
}

export default AppPage
