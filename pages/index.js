import STAppBar from '../components/app-bar'
import { Box, Card, CardActionArea, CardHeader, CardMedia, Grid, Typography } from '@mui/material'
import { useEffect, useState } from 'react'
import { getChild } from '../utils/services/user-service'
import { useQuery } from 'react-query'
import { API_URL } from '../utils/constants'
import axios from 'axios'
import { useRouter } from 'next/router'


const HomePage = () => {

    const router = useRouter()

    const { data: tasks } = useQuery('tasks', async () => {
        const tasks = await axios.get(API_URL + 'task')
        return tasks.data
    })

    const { data: taskVariants } = useQuery('taskVariants', async () => {
        const taskVariants = await axios.get(API_URL + 'task-variant')

        console.log(taskVariants.data)
        return taskVariants.data
    })

    const [childName, setChildName] = useState('')
    useEffect(() => {
        setChildName(getChild().name)
    }, [])

    return (
        <>
            <STAppBar text={'Задания'} child/>
            <Box p={2}>
                <Typography variant={'h4'} component={'p'} sx={{ textAlign: 'center' }}>
                    Выберите задание:
                </Typography>
                <Box mt={1}>
                    {tasks &&
                        tasks.map((task) =>
                            (<Box key={task.id}>
                                <Typography variant={'h4'} component={'p'} pb={1}>
                                    {task.title}
                                </Typography>
                                {/*<Typography variant={'subtitle1'} component={'p'}>*/}
                                {/*    {task.desc}*/}
                                {/*</Typography>*/}
                            </Box>),
                        )
                    }
                </Box>
                <Grid container spacing={2}>
                    {tasks && taskVariants &&
                        taskVariants.map((variant) => (
                            <Grid item xs={6} key={variant.id}>
                                <Card
                                    onClick={() => {
                                        router.push({ pathname: 'app', query: { task: 'http://95.163.235.148:9000/files/' + variant.variantPath } })
                                    }}
                                >
                                    <CardActionArea>
                                        <Typography variant={'h4'} ml={1} mt={1} sx={{color: '#2b79f3'}}>
                                            {`№ ${variant.order}`}
                                        </Typography>
                                        <CardMedia
                                            component={'img'}
                                            height={'210'}
                                            image={'http://95.163.235.148:9000/files/' + variant.variantPath}
                                        />
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        ))
                    }
                </Grid>
            </Box>
        </>
    )
}

export default HomePage