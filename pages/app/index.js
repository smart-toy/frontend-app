import React, { useRef } from 'react'
import { Box, Button, IconButton, Typography } from '@mui/material'
import { ReactSketchCanvas } from 'react-sketch-canvas'
import { useRouter } from 'next/router'
import usePageRedirect from '../../utils/use-page-redirect'
import UndoIcon from '@mui/icons-material/Undo';


const App = () => {
    const canvas = useRef()
    const { query } = useRouter()
    const toHome = usePageRedirect('/')

    const exportSvg = async () => {
        canvas.current
            .exportSvg('jpeg')
            .then(data => {
                // console.log(data)
                // navigator.clipboard.writeText(data.toString()).then(() => alert('copied to clipboard!'))
                toHome()
            })
            .catch(e => {
                console.log(e)
            })
    }

    const undoCanvas = () => {
        canvas.current
            .undo()
    }

    return (
        <Box position={'relative'} height={'100vh'} width={'100vw'}>
            {
                query &&
                <ReactSketchCanvas
                    ref={canvas}
                    strokeWidth={8}
                    width={'640px'}
                    height={'440px'}
                    strokeColor={'orange'}
                    backgroundImage={query.task}
                    preserveBackgroundImageAspectRatio={'xMinYMin'}
                    exportWithBackgroundImage={false}
                    style={{
                        border: 'none',
                        backgroundRepeat: 'repeat-x',
                    }}
                />
            }
            <Button size={'large'} variant="contained" onClick={exportSvg}
                    sx={{
                        position: 'absolute',
                        bottom: 8,
                        right: 16,
                        background: 'green',
                    }}
            >
                <Typography variant={'h6'} sx={{letterSpacing: 1, fontWeight: 600}}>
                    Готово
                </Typography>
            </Button>
            <IconButton size={'large'} variant="contained" onClick={undoCanvas}
                    sx={{
                        position: 'absolute',
                        top: 8,
                        left: 16,
                        border: '4px solid orange'
                    }}
            >
                {/*<Typography variant={'subtitle1'} sx={{color: '#eee'}}>*/}
                {/*    */}
                {/*</Typography>*/}
                <UndoIcon sx={{color: 'orange', fontSize: '3rem'}} />
            </IconButton>
        </Box>
    )
}

export default App
