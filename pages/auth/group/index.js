import React from 'react'
import STAppBar from '../../../components/app-bar'
import { Box, Card, CardActionArea, Skeleton, Stack, Typography } from '@mui/material'
import { useQuery } from 'react-query'
import axios from 'axios'
import { API_URL } from '../../../utils/constants'
import { setGroup } from '../../../utils/services/user-service'
import { useRouter } from 'next/router'


const AuthPage = () => {

    const { isLoading, isError, data: groups } = useQuery('groups', async () => {
        const response = await axios.get(API_URL + 'group')
        return response.data
    })

    const router = useRouter()

    return (
        <>
            <STAppBar text={'Выбор группы'}/>
            <Box mx={3} mt={3}>
                <Typography variant={'h4'} sx={{ textAlign: 'center' }} mt={2}>
                    Выберите группу:
                </Typography>
                <Stack direction={'row'} spacing={2} mt={2} justifyContent={'space-between'}>
                    {isLoading &&
                        <>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                        </>
                    }
                    {isError &&
                        <Typography variant={'h5'} sx={{ textAlign: 'center' }} mt={2}>
                            Ошибка! Не удалось получить группы
                        </Typography>
                    }
                    {groups &&
                        groups.map((group) =>
                            <Card
                                key={group.id}
                                onClick={() => {
                                    setGroup(group.id, group.name)
                                    router.push({ pathname: 'child', query: { group: group.id, groupName: group.name } })
                                }}
                            >
                                <CardActionArea>
                                    <Box
                                        p={2}
                                        sx={{
                                            background: '#e9f4ff',
                                        }}
                                    >
                                        <Typography variant={'h4'} textAlign={'center'}>
                                            {group.name}
                                        </Typography>
                                    </Box>
                                </CardActionArea>
                            </Card>,
                        )
                    }
                </Stack>
            </Box>
        </>
    )
}

export default AuthPage
