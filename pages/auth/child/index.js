import React from 'react'
import STAppBar from '../../../components/app-bar'
import { Box, Card, CardActionArea, Skeleton, Stack, Typography } from '@mui/material'
import { useQuery } from 'react-query'
import axios from 'axios'
import { API_URL } from '../../../utils/constants'
import { setChild, signInChild } from '../../../utils/services/user-service'
import { useRouter } from 'next/router'
import usePageRedirect from '../../../utils/use-page-redirect'


const AuthPage = () => {

    const { query } = useRouter()
    const toMain = usePageRedirect('/')

    const { isLoading, isError, data: children } = useQuery('children', async () => {
        const groupId = await query.group
        const childs = await axios.get(API_URL + 'child')
        const users = await axios.get(API_URL + 'user')

        const userIds = childs.data.map((child) => {
            if (child.groupId === groupId) {
                return child.userId
            }
        })

        return users.data.filter((user) => userIds.indexOf(user.id) !== -1)
    }, { enabled: !!query.group })

    return (
        <>
            <STAppBar text={'Выбор ребенка'}/>
            <Box mx={3} mt={3}>
                <Typography variant={'h4'} mt={2} textAlign={'center'}>
                    {`${query.groupName}. Имя ребенка`}
                </Typography>
                <Stack direction={'row'} spacing={2} mt={2}>
                    {isLoading &&
                        <>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                            <Skeleton variant="rectangular" width={256} height={100}/>
                        </>
                    }
                    {isError &&
                        <Typography variant={'h5'} sx={{ textAlign: 'center' }} mt={2}>
                            Ошибка! Не удалось получить группы
                        </Typography>
                    }
                    {children &&
                        children.map((child, index) =>
                            <Card
                                key={child.id}
                                onClick={() => {
                                    setChild(child.id, child.name)
                                    signInChild(child.email)
                                    toMain()
                                }}
                            >
                                <CardActionArea>
                                    <Box
                                        p={2}
                                        sx={{
                                            background: '#e9f4ff',
                                        }}
                                    >
                                        <Typography textAlign={'center'} variant={'h4'}>
                                            {`${index+ 1}. ${child.name} ${child.surname}`}
                                        </Typography>
                                    </Box>
                                </CardActionArea>
                            </Card>,
                        )
                    }
                </Stack>
            </Box>
        </>
    )
}

export default AuthPage
