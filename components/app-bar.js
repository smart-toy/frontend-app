import React from 'react'
import { AppBar, Toolbar, Typography } from '@mui/material'


const STAppBar = ({ text, child }) => {
    return (
        <AppBar position={'static'}>
            <Toolbar>
                <Typography variant={'h4'}>
                    {child ? '👶' : '👨‍🏫'} Smart toy - {text}
                </Typography>
            </Toolbar>
        </AppBar>
    )
}

export default STAppBar
