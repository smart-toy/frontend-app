import axios from 'axios'
import { API_URL } from '../constants'


export const setGroup = (groupId, groupName) => {
    sessionStorage.setItem('groupId', groupId)
    sessionStorage.setItem('groupName', groupName)
}

export const getGroup = () => {
    if (typeof window === 'undefined') return {id: '-1', name: '-1'}

    const id = sessionStorage.getItem('groupId')
    const name = sessionStorage.getItem('groupName')
    return {id, name}
}

export const setChild = (userId, childName) => {
    sessionStorage.setItem('userId', userId)
    sessionStorage.setItem('childName', childName)
}

export const getChild = () => {
    if (typeof window === 'undefined') return {id: '-1', name: '-1'}

    const id = sessionStorage.getItem('userId')
    const name = sessionStorage.getItem('childName')
    return {id, name}
}

export const signInChild = (email) => {
    console.log({email, password: 'testchild1'})
    axios.post(API_URL + 'auth/login', {email, password: 'testchild1'})
        .then((res) => {
            console.log(res)
            localStorage.setItem('accessToken', res.data.accessToken)
            localStorage.setItem('refreshToken', res.data.refreshToken)
        })
        .catch((err) => console.error(err))
}
